package com.inspedio.samples;

import com.inspedio.entity.InsSprite;
import com.inspedio.entity.InsState;
import com.inspedio.entity.InsText;
import com.inspedio.entity.actions.MoveBy;
import com.inspedio.entity.actions.MoveTo;
import com.inspedio.entity.primitive.InsCallback;
import com.inspedio.entity.sprite.InsAnimatedSprite;
import com.inspedio.entity.ui.InsButton;
import com.inspedio.enums.FontSize;
import com.inspedio.enums.FontStyle;
import com.inspedio.enums.HAlignment;
import com.inspedio.enums.KeyCode;
import com.inspedio.enums.VAlignment;
import com.inspedio.system.core.InsGlobal;
import com.inspedio.system.helper.InsCamera;
import com.inspedio.system.helper.InsKeys;
import com.inspedio.system.helper.InsUtil;

public class SampleCameraState extends InsState{

	InsAnimatedSprite sprite;
	InsSprite bg;
	InsText info;
	InsCamera cam;
	
	public void create() {
		// Set Background to show camera usage
		int boundary = 300;
		
		bg = new InsSprite("/com/inspedio/sample/bg.png", 0, 0);
		this.add(bg);
		
		sprite = new InsAnimatedSprite("/com/inspedio/sample/sprite.png", 0, 0, 32, 48);
		int frameDelay = 5;
		
		sprite.addAnimation("DOWN", new int[]{0, 1, 2, 3}, frameDelay);
		sprite.addAnimation("LEFT", new int[]{4, 5, 6, 7}, frameDelay);
		sprite.addAnimation("RIGHT", new int[]{8, 9, 10, 11}, frameDelay);
		sprite.addAnimation("UP", new int[]{12, 13, 14, 15}, frameDelay);
		sprite.addAnimation("IDLE", new int[]{0});
		this.add(sprite);
		
		// Set Camera Boundary and Follow Object
		cam = InsGlobal.camera;
		cam.setBoundary(-boundary, boundary, -boundary, boundary);
		cam.follow(sprite);
		
		info = new InsText("", InsGlobal.middleX, 20);
		info.setAlignment(HAlignment.CENTER, VAlignment.TOP);
		this.add(info);
		
		InsButton back = new InsButton(30, InsGlobal.screenHeight - 20, 60, 40, "BACK", 0xFFFFFF);
		back.setBorder(0, 3);
		back.setCaption("BACK", 0, FontSize.LARGE, FontStyle.BOLD);
		back.setClickedCallback(new InsCallback() {
			public void call() {
				onLeftSoftKey();
			}
		});
		
		this.add(back);
	}
	
	public void onLeftSoftKey()
	{
		InsGlobal.switchState(new SampleButtonState(), false);
	}
	
	public void postUpdate(){
		super.postUpdate();
		this.refreshCameraInfo();
	}
	
	public void refreshCameraInfo(){
		this.info.setText(new String[] {"[Camera]", "Left:" + cam.getLeft() + " Right:" + cam.getRight(), "Top:" + cam.getTop() + " Down:" + cam.getBottom()});
	}
	
	public boolean onPointerPressed(int X, int Y) {
		this.moveSpriteTo(X + cam.getLeft(), Y + cam.getTop());
		
		return super.onPointerPressed(X, Y);
	}
	
	public void moveSpriteTo(int X, int Y){
		this.sprite.setAction(MoveTo.create(this.sprite, 25, X, Y));
		
		int deltaX = X - sprite.position.x;
		int deltaY = Y - sprite.position.y;
		if(InsUtil.Absolute(deltaX) >= InsUtil.Absolute(deltaY)){
			if(deltaX > 0) {sprite.playAnimation("RIGHT");}
			else {sprite.playAnimation("LEFT");}
		} else {
			if(deltaY > 0) {sprite.playAnimation("DOWN");}
			else {sprite.playAnimation("UP");}
		}
	}
	
	public void handleKeyState(InsKeys key)
	{
		if(key.keyPressed(KeyCode.UP)){
			sprite.playAnimation("UP");
			sprite.setAction(MoveBy.create(10, 0, -100, null));
		}
		
		if(key.keyPressed(KeyCode.DOWN)){
			sprite.playAnimation("DOWN");
			sprite.setAction(MoveBy.create(10, 0, 100, null));
		}
		
		if(key.keyPressed(KeyCode.LEFT)){
			sprite.playAnimation("LEFT");
			sprite.setAction(MoveBy.create(10, -100, 0, null));
		}
		
		if(key.keyPressed(KeyCode.RIGHT)){
			sprite.playAnimation("RIGHT");
			sprite.setAction(MoveBy.create(10, 100, 0, null));
		}
		
		if(sprite.action == null){
			sprite.playAnimation("IDLE");
		}
	}

}
